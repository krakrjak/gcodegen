module Main where

import GCodeGen (writeVolumeGCode, cube, tube, pyramid, dome, gear, chalice)

import Linear (V3(V3))

import Data.Maybe (fromMaybe)
import Text.Read (readMaybe)

data ObjectType = Cube | Tube | Pyramid | Dome | Gear | Chalice
  deriving (Show,Read,Eq,Ord)

main :: IO ()
main = do
  putStrLn "Invalid inputs will yield the default value."

  putStrLn "Do want to generate gcode for (Default: (0)cube):"
  putStrLn "  (0)cube"
  putStrLn "  (1)tube"
  putStrLn "  (2)pyramid"
  putStrLn "  (3)dome"
  putStrLn "  (4)gear"
  putStrLn "  (5)chalice"
  objectTypeString <- getLine
  let objectType = case objectTypeString of
        "0" -> Cube
        "cube" -> Cube
        "Cube" -> Cube
        "1" -> Tube
        "tube" -> Tube
        "Tube" -> Tube
        "2" -> Pyramid
        "pyramid" -> Pyramid
        "Pyramid" -> Pyramid
        "3" -> Dome
        "dome" -> Dome
        "Dome" -> Dome
        "4" -> Gear
        "gear" -> Gear
        "Gear" -> Gear
        "5" -> Chalice
        "chalice" -> Chalice
        "Chalice" -> Chalice
        _ -> Cube
  putStrLn ("Object type: " ++ show objectType)

  putStrLn "Voxel size in X direction in millimeters. How far to move the laser for each step. Default: 0.20"
  voxelSizeXString <- getLine
  let voxelSizeX = fromMaybe 0.20 (readMaybe voxelSizeXString)
  putStrLn ("Voxel size in X direction: " ++ show voxelSizeX)

  putStrLn "Voxel size in Y direction in millimeters. How far to move the bed after each scanline. Default: 0.20"
  voxelSizeYString <- getLine
  let voxelSizeY = fromMaybe 0.20 (readMaybe voxelSizeYString)
  putStrLn ("Voxel size in Y direction: " ++ show voxelSizeY)

  putStrLn "For your information, the voxel size in Z direction (the layer height) is hardcoded to be 0.06."

  putStrLn "The speed of the laser in millimeters per minute. Default: 1200.0"
  speedXString <- getLine
  let speedX = fromMaybe 1200.0 (readMaybe speedXString)
  putStrLn ("Laser speed: " ++ show speedX)

  let voxelSize = V3 voxelSizeX voxelSizeY 0.06

  case objectType of
    Cube -> writeVolumeGCode "cube.gcode" speedX (cube voxelSize)
    Tube -> writeVolumeGCode "tube.gcode" speedX (tube voxelSize)
    Pyramid -> writeVolumeGCode "pyramid.gcode" speedX (pyramid voxelSize)
    Dome -> writeVolumeGCode "dome.gcode" speedX (dome voxelSize)
    Gear -> writeVolumeGCode "gear.gcode" speedX (gear voxelSize)
    Chalice -> writeVolumeGCode "chalice.gcode" speedX (chalice voxelSize)

