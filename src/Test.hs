module Main where

import GCodeGen (
  Volume(Volume), GCode, produceGCode,
  feedMaterial, scanlinesGCode,
  cube, tube, pyramid, dome, gear, chalice)

import Linear (V3(V3))
import Pipes (Producer)

import Test.Tasty (defaultMain, TestTree, testGroup)
import Test.Tasty.Golden (goldenVsFileDiff)

import System.IO (withFile, IOMode(WriteMode), hPutStrLn, hPutStr)
import Control.Monad (forM_, replicateM_)

main :: IO ()
main = do
  defaultMain (testGroup "tests" [
    testGroup "gcode" [
      testGCodeProducer "feedMaterial5" (feedMaterial 5),
      testGCodeProducer "scanlinesGCodeTwoLines" scanlinesGCodeTwoLines,
      testGCodeProducer "scanlinesGCodeOneLine" scanlinesGCodeOneLine],
    testGroup "volume" [
      testVolume "cube" (cube (V3 1 1 1)),
      testVolume "tube" (tube (V3 0.5 0.5 0.5)),
      testVolume "pyramid" (pyramid (V3 1 1 1)),
      testVolume "dome" (dome (V3 1 1 1)),
      testVolume "gear" (gear (V3 0.5 0.5 0.5)),
      testVolume "chalice" (chalice (V3 0.5 0.5 0.5))]])

testGCodeProducer :: String -> Producer GCode IO () -> TestTree
testGCodeProducer name producer = goldenVsFileDiff
  name
  (\ref new -> ["diff", "-u", ref, new])
  ("tests/gcode/" ++ name ++ ".golden")
  ("tests/gcode/" ++ name ++ ".out")
  (produceGCode ("tests/gcode/" ++ name ++ ".out") producer)

writeVolume :: FilePath -> Volume -> IO ()
writeVolume filePath (Volume voxelSize voxelData) = do
  withFile filePath WriteMode (\handle -> do
    hPutStrLn handle (show voxelSize)
    forM_ voxelData (\layer -> do
      forM_ layer (\scanline -> do
        forM_ scanline (\(stride,voxel) ->
          replicateM_ stride (
            if voxel then hPutStr handle "X" else hPutStr handle "."))
        hPutStr handle "\n")
      hPutStr handle "\n"))

testVolume :: String -> Volume -> TestTree
testVolume name volume = goldenVsFileDiff
  name
  (\ref new -> ["diff", "-u", ref, new])
  ("tests/volume/" ++ name ++ ".golden")
  ("tests/volume/" ++ name ++ ".out")
  (writeVolume ("tests/volume/" ++ name ++ ".out") volume)

scanlinesGCodeTwoLines :: (Monad m) => Producer GCode m ()
scanlinesGCodeTwoLines = scanlinesGCode 1200 (V3 1 1 1) [
  [(5,False),(6,True),(7,False)],
  [(5,False),(13,True)]]

scanlinesGCodeOneLine :: (Monad m) => Producer GCode m ()
scanlinesGCodeOneLine = scanlinesGCode 1200 (V3 1 1 1) [
  [(3,False),(4,True),(3,False)]]
