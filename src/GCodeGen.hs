module GCodeGen where

import Linear (V3(V3))
import System.IO (withFile, IOMode(WriteMode))
import Numeric (showFFloat)
import Data.Complex (Complex((:+)),polar)
import Pipes (Producer, yield, each, runEffect, (>->))
import Pipes.Prelude (toHandle)
import Data.List (group)
import Control.Monad (forM_, replicateM_)



produceGCode :: FilePath -> Producer GCode IO () -> IO ()
produceGCode filepath gcodeProducer = withFile filepath WriteMode (\handle ->
  runEffect (gcodeProducer >-> toHandle handle))

-- | Write GCode for the given volume to the given file path.
writeVolumeGCode :: FilePath -> SpeedX -> Volume -> IO ()
writeVolumeGCode filepath speedX volume =
  produceGCode filepath (volumeGCode speedX volume)



data Volume = Volume { _voxelSize :: VoxelSize, _voxelData :: VolumeData}
type VolumeData = [Layer]
type Layer = [ScanLine]

-- | A scan line is represented as a list of strides.
-- A strides is a pair of a number and a bool. The number is the
-- number of times the bool occurs in a row.
type ScanLine = [(Int,Bool)]

-- | The size of our smallest printable unit in x-, y- and z-direction
-- in millimeters.
type VoxelSize = V3 Double

-- | How fast the laser moves in millimeter per second.
type SpeedX = Double

-- | A single line of GCode.
type GCode = String


-- | The amount of powder to feed to prime the material feeding wheel
prepDistance :: Double
prepDistance = 40.0


-- | For now this is a magic value that will determine the layer height of the
-- prints.  Larger numbers indicate more compaction of material.  This means
-- that the compaction roller (T1 E axis) will move less often when the feed roller
-- (T0 E axis) moves. Example: if the compationRatio is 1.0 then T1 E will move
-- exactly as much as T0 E.  As the compationRatio becomes larger T1 E will move less
-- often.
--
-- The machine as built has a limitation where we can only make the ratio as large as
-- 20.0 due to issues in trying to move the compaction roller any less than 0.01mm or
-- 1 step.  Values smaller than 20.0 lead to larger layer heights in the print and
-- values smaller than 10.0 lead to powder falling off the roller prematurely.
--
-- This value has been designed away in future iterations of the machine.
-- In future iterations, this value will be controlled mechanically
-- and will disappear from the codebase.  We know that a compation/compression
-- amount of 20.0 results in a layer height between 0.055mm and 0.06mm.
compactionRatio :: Double
compactionRatio = 20.0



-- Each axis on the machine
-- will have different feed rates as they each contain differing types of mechanics.
-- Feeding of material works up to 500 mm/min
-- Feeding of the X axis works up to 12,000 mm/min
-- Feeding of the Y axis works up to 300 mm/min
-- Feeding of the Z axis works up to 200 mm/min


-- | The power of the laser on a scale of 0.0 to 1.0.
power :: Double
power = 1.0


-- | Given a volume generate the `GCode` that prints it.
volumeGCode :: (Monad m) => SpeedX -> Volume -> Producer GCode m ()
volumeGCode speedX volume = do
  setupMachine
  feedMaterial prepDistance
  moveToBuildArea
  sequence_ (zipWith (layerGCode speedX (_voxelSize volume)) [1..] (_voxelData volume))


-- | Generate the data for a cube of size 10mm * 10mm * 10mm.
-- Uses the given voxel size to determine the resolution.
cube :: VoxelSize -> Volume
cube = generateVolume (V3 0 0 0) (V3 10 10 10) (const True)


-- | A tube of size 10mm*10mm*10mm. Uses the given voxel size
-- to determine the resolution.
tube :: VoxelSize -> Volume
tube = generateVolume (V3 (-5) (-5) 0) (V3 5 5 10) f where
  f (V3 x y _) = d > 2.5 && d <= 5 where
    d = sqrt (x * x + y * y)

-- | A pyramid with a quadratic base of size 10mm*10mm*10mm.
-- Uses the given voxel size to determing the resolution.
pyramid :: VoxelSize -> Volume
pyramid = generateVolume (V3 (-5) (-5) 0) (V3 5 5 10) f where
  f (V3 x y z) = abs x + abs y <= 5 - 0.5 * z

-- | A dome (the upper half of a ball) of size 10mm*10mm*5mm.
-- Uses the given voxel size to determing the resolution.
dome :: VoxelSize -> Volume
dome = generateVolume (V3 (-5) (-5) 0) (V3 5 5 5) f where
  f (V3 x y z) = sqrt (x * x + y * y + z * z) <= 5

-- | A gear with eight teeth of size 10mm*10mm*5mm.
-- Uses the given voxel size to determing the resolution.
gear :: VoxelSize -> Volume
gear = generateVolume (V3 (-5) (-5) 0) (V3 5 5 5) f where
  f (V3 x y _) = insideRing || insideTooth where
    insideRing = d > 2.0 && d <= 3.5
    insideTooth = d > 3.5 && d <= 5.0 && evenSegment
    evenSegment = even (floor ((p / tau + 0.5) * 16) :: Int)
    (d,p) = polar (x :+ y)
    tau = 2 * pi

-- | The union of a small platorm and the lower half of a ball.
chalice :: VoxelSize -> Volume
chalice = generateVolume (V3 (-5) (-5) 0) (V3 5 5 5) f where
  f (V3 x y z) = insidePlatform || insideBall where
    insidePlatform = z <= 0.5
    insideBall = sqrt (x * x + y * y + (z - 5) * (z - 5)) <= 5


-- | Generate a body from its extent and a test function. Generates
-- the scanlines point for point and then does run length encoding to
-- get a list of strides.
generateVolume :: V3 Double -> V3 Double -> (V3 Double -> Bool) -> VoxelSize -> Volume
generateVolume lower upper f voxelSize =
  Volume voxelSize voxelData where
    V3 lowerX lowerY lowerZ = lower
    V3 upperX upperY upperZ = upper
    V3 voxelSizeX voxelSizeY voxelSizeZ = voxelSize
    voxelData = do
      z <- enumFromStepTo lowerZ voxelSizeZ upperZ
      return (do
        y <- enumFromStepTo lowerY voxelSizeY upperY
        return (runLengthEncode (do
          x <- enumFromStepTo lowerX voxelSizeX upperX
          return (f (V3 x y z)))))


enumFromStepTo :: Double -> Double -> Double -> [Double]
enumFromStepTo from step to = [from, next .. to] where
  next = from + step


runLengthEncode :: (Eq a) => [a] -> [(Int,a)]
runLengthEncode xs = map (\x -> (length x,head x)) (group xs)

-- | Prepare the machine for operation.
setupMachine :: (Monad m) => Producer GCode m ()
setupMachine = each [";Powder Machine Preamble -- Setup"
                   ,"G21       ; Force millimeter mode"
                   ,"M203 Z200 ; Set Z max speed to 60mm/min"
                   ,"T0 G92 E0 ; Set wheel to position 0"
                   ,"T0 M83    ; Set relative mode for wheel"
                   ,"T1 G92 E0 ; Set compressor to position 0"
                   ,"T1 M83    ; Set relative mode for compressor"
                   ,"G91       ; Set relative motion for XYZ"
                   ,"G0 Z5 F60 ; Move bed down 5mm for homing action"
                   ,"G28 Y0    ; Home the Y axis"
                   ,"G92 X0 Y0 ; Force X and Y to be 0 after homing operation"
                   ]

-- | Feed a line of material with the given width. If the width
-- is big the line is more like a plane. Also compresses the material
-- using the hard-coded `compactionRatio` constant. Does some strange
-- and magical calculations.
feedMaterial :: (Monad m) => Double -> Producer GCode m ()
feedMaterial distance = do

  comment ("Feeding " ++ show distance ++ " mm of material with compression " ++ show compactionRatio)

  let compressCount = floor ((distance / compactionRatio) / fixedPoint)
      dUntilCompress = distance / fromIntegral compressCount
      feedSpeed = 300.0 :: Double
      fixedPoint = 0.01

  replicateM_ compressCount (do
    yield ("T0 G0 E" ++ showF2 dUntilCompress ++ " F" ++ show feedSpeed ++ " ; Feed wheel")
    yield ("T1 G0 E" ++ showF2 fixedPoint ++ " F" ++ show feedSpeed ++ " ; Compress material"))


-- | Assume the Build area starts at Y=50mm or 5cm from home
moveToBuildArea :: (Monad m) => Producer GCode m ()
moveToBuildArea = do
  comment "Move to build area"
  yield ("G0 Y50 F500")
  comment "Enable the laser"
  yield ("M106")
  comment "Raise the bed to begin the print"
  yield ("G0 Z-5")


-- | Produce `GCode` for a single layer. Takes the voxel size, the layer
-- number and the layer. After each printed layer it moves the print bed
-- up, feeds some material into the air, moves the print bed back in the
-- Y direction and moves it back up.
layerGCode :: (Monad m) => SpeedX -> VoxelSize -> Int -> Layer -> Producer GCode m ()
layerGCode speedX voxelSize@(V3 _ voxelSizeY voxelSizeZ) number layer = do

  comment ("Ready to print layer number " ++ show number)

  scanlinesGCode speedX voxelSize layer

  -- Turn the laser off before moving the Z out of the way
  yield ("M107 ; Disable the laser when moving away")

  -- Move the print bed out of the way so that when we move in the Y axis powder
  -- does not transfer onto the build surface or scrape off on the part.
  yield ("G0 Z5 ; Get the powder off the part")

  -- Feed material so that we have no overlap in the previously imaged portion of
  -- the powder roller.  This was experimentally verified to be the only way to
  -- ensure reliable printing.  This magic number can be as small as 1mm, but 5mm
  -- gave the best tradeoff in terms of speed of feeding and accuracy of the next
  -- layer
  feedMaterial 5

  -- The Y size of each layer.
  let layerSizeY = fromIntegral (length layer) * voxelSizeY

  -- Move the machine all the way back along the Y axis for the next layer.
  yield ("G0 Y-" ++ showF5 layerSizeY ++ " ; Reset to start")

  -- Enable the laser before moving back up
  yield ("M106 ; Enable the laser")

  -- Move the print bed back up leaving space for the next layer.
  yield ("G0 Z" ++ showF2 (voxelSizeZ - 5.0) ++ " ; Move back up minus layer height")

-- | Generate `GCode` for the given list of `ScanLine`s. Takes the
-- the speed of the laser, the voxel size and a list of scanlines.
-- Generates `GCode` for each pair of lines because after the laser
-- moved all the way to one side we want to shoot the next line on
-- the way back. If there was an odd number of scan lines we move
-- the laser back to its original position after the last line.
scanlinesGCode :: (Monad m) => SpeedX -> VoxelSize -> [ScanLine] -> Producer GCode m ()

scanlinesGCode _ _ [] = return ()

scanlinesGCode speedX voxelSize@(V3 voxelSizeX _ _) (lastScanLine:[]) = do
  scanlineGCodePositive speedX voxelSize lastScanLine
  let scanLineWidth = fromIntegral (sum (map fst lastScanLine)) * voxelSizeX
  yield ("G0 X-" ++ showF5 scanLineWidth ++ " F" ++ show speedX ++ " ; Reset movement to beginning of line")

scanlinesGCode speedX voxelSize (firstScanLine:secondScanLine:otherScanLines) = do
  scanlineGCodePositive speedX voxelSize firstScanLine
  scanlineGCodeNegative speedX voxelSize secondScanLine
  scanlinesGCode speedX voxelSize otherScanLines

-- | Generate the GCode for a single scanline in the positive X direction.
-- Takes the laser speed, the voxel size and a scanline.
-- Before printing the line it feeds material.
-- After printing the line it moves the print bed in the Y direction.
scanlineGCodePositive :: (Monad m) => SpeedX -> VoxelSize -> ScanLine -> Producer GCode m ()
scanlineGCodePositive speedX (V3 voxelSizeX voxelSizeY _) scanline = do
  feedMaterial voxelSizeY
  forM_ scanline (voxelStrideGCode speedX voxelSizeX)
  yield ("G0 Y" ++ showF5 voxelSizeY)


-- | Generate the GCode for a single scanline in the negative X direction.
-- Takes the laser speed, the voxel size and a scanline.
-- Before printing the line it feeds material.
-- After printing the line it moves the print bed in the Y direction.
scanlineGCodeNegative :: (Monad m) => SpeedX -> VoxelSize -> ScanLine -> Producer GCode m ()
scanlineGCodeNegative speedX (V3 voxelSizeX voxelSizeY _) scanline = do
  feedMaterial voxelSizeY
  forM_ (reverse scanline) (voxelStrideGCode speedX (negate voxelSizeX))
  yield ("G0 Y" ++ showF5 voxelSizeY)



-- | Generate the GCode for a stride of voxels. Takes the speed of the
-- laser, the distance the laser has to move for a single voxel and a
-- pair of the number of voxels and a bool. The bool is true if
-- we want to shoot the laser and false otherwise.
-- If the movement distance is positive we move in the positive X direction
-- and if it is negative we move in the negative X direction.
-- The G1 code moves and shoots the laser
-- The G0 code moves and does not shoot the laser
voxelStrideGCode :: (Monad m) => SpeedX -> Double -> (Int,Bool) -> Producer GCode m ()
voxelStrideGCode speedX stepDistance (stride,True) = do
  let movementDistance = fromIntegral stride * stepDistance
  yield ("G1 X" ++ showF5 movementDistance ++ " F" ++ show speedX ++ " S" ++ showF1 power ++ " ; Shoot the laser")
voxelStrideGCode speedX stepDistance (stride,False) = do
  let movementDistance = fromIntegral stride * stepDistance
  yield ("G0 X" ++ showF5 movementDistance ++ " F" ++ show speedX ++ " ; Move the laser")



-- | Emit a `GCode` comment.
comment :: (Monad m) => String -> Producer GCode m ()
comment message = yield ("; " ++ message)

-- | Why do we ouput different numbers with different precisions?
-- This was due to issues with number stability.  When requesting Just 5
-- sometimes there would be a dangling 1 in the 5th position with intervening 0
-- Example: 0.20001, when we really just wanted 0.2 or even 0.20 would be acceptable
-- Perhaps this oddity can be deisgned away...
showF1 :: Double -> String
showF1 x = showFFloat (Just 1) x ""

showF2 :: Double -> String
showF2 x = showFFloat (Just 2) x ""

showF5 :: Double -> String
showF5 x = showFFloat (Just 5) x ""

